<?php

namespace Mansa\Simplepay;

class GetDefaultParameters {
	
	/*
	* default parameter
	*/
	private $parameters;

	
	public function getDefaultParameters(){
    	return array(
    		"userId"=>'',
    		"password"=>'',
    		'entityId'=>'',
    		);
    }

    /**
     * Get a single parameter.
     *
     * @param string $key The parameter key
     * @return mixed
     */
    protected function getParameter($key)
    {
        return $this->parameters->get($key);
    }


    /**
     * Set a single parameter.
     *
     * @param string $key The parameter key
     * @param string $value The parameter value
     * @return mixed
     */
    protected function setParameter($key,$value)
    {
    	return $this->setParameter->set($key,$value);
    }

    
    public function getUser(){
    	return $this->getParameter('userId');
    }

    public function setUser($user){
    	return $this->setParameter("userId",$user);
    }
    public function getPassword(){
    	return $this->getParameter("password");
    }
    public function setPassword($password){
    	return $this->setParameter("password",$password);
    }
    public function setEntity($entityId){
    	return $this->setParameter("entityId",$entityId);
    }
    public function getEntity(){
    	return $this->getParameter("entityId");
    }


}