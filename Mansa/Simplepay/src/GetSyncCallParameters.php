<?php

namespace Mansa\Simplepay;

class GetSyncCallParameters extends GetDefaultParameters{
	

	private $parameters;


	public function GetDefaultParameters(){
		$settings = parent::GetDefaultParameters();
		$settings['amount']='';
		$settings['currency']='';
		$settings['paymentBrand']='';
		$settings['paymentType']='';
		$settings['cardnumber']='';
		$settings['cardholder']='';
		$settings['cardexpiryMonth']='';
		$settings['cardexpiryYear']='';
		$settings['cardcvv']='';
		$settings['environment']='';
		return $settings;
	}

	public function getAmount($amount){
		return $this->getParameter($amount);
	}
	public function getCurreny($currency){
		return $this->getParameter($currency);
	}
	public function getPaymentBrand($paymentBrand){
		return $this->getParameter($paymentBrand);
	}
	public function getPaymentType($paymentType){
		return $this->getParameter($paymentType);
	}
	public function getCardNumber($cardNumber){
		return $this->getParameter($cardNumber);
	}
	public function getCardHolder($cardHolder){
		return $this->getParameter($cardHolder);
	}
	public function getCardExpMonth($cardExpiryMonth){
		return $this->getParameter($cardExpiryMonth);
	}
	public function getCardExpYear($cardExpiryYear){
		return $this->getParameter($cardExpiryYear);
	}
	public function getCvv($cvv){
		return $this->getParameter($cvv);
	}
	public function getEnvironment($environment){
		return $this->getParameter($environment);
	}
	public function setAmount($amount){
		return $this->setParameter("amount",$amount);
	}
	public function setCurrency($currency){
		return $this->setParameter("currency",$currency);
	}
	public function setPaymentBrand($paymentBrand){
		return $this->setParameter("paymentBrand",$paymentBrand);
	}
	public function setPaymentType($paymentType){
		return $this->setParameter("paymentType",$paymentType);
	}
	public function setCardNumber($cardNumber){
		return $this->setParameter("cardnumber",$cardNumber);
	}
	public function setCardHolder($cardHolder){
		return $this->setParameter("cardholder",$cardHolder);
	}
	public function setCardExpMonth($cardExpiryMonth){
		return $this->setParameter("cardexpiryMonth",$cardExpiryMonth);
	}
	public function setCardExpYear($cardExpiryYear){
		return $this->setParameter("cardexpiryYear",$cardExpiryYear);
	}
	public function setCVV($cvv){
		return $this->setParameter("cardcvv",$cvv);
	}
	public function setEnvironment($environment){
		return $this->setParameter("environment",$environment);
	}
	
	public function getAllParameters(){
		return $this->parameters->all();
	}
	
	

	
	
	

	



}