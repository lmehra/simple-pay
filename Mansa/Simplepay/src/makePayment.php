<?php

namespace Mansa\Simplepay;

use Simplepay\GetSyncCallParameters;
use Simplepay\GetDefaultParameters;
use Simplepay\Exceptions\VariableValidationException;
use Simplepay\Exceptions\PaymentGatewayVerificationFailedException;
use Simplepay\Exceptions\UnknownPaymentGatewayException;

class makePayment{
	
	protected $testEndpoint = "http://test.oppwa.com/";
	protected $liveEndpoint = "https://oppwa.com/";
	protected $verison = 'v1';
	protected $api_ = '/payments';
	private $url ='';

	public function __construct(){
		$env = $this->getPareneter('environment');
		$this->url = $env == 'live'?$liveEndpoint:$testEndpoint;
	}

	public function getAmount($amount){
		$amount =  $this->getParameter($amount);
	}
	public function getCurreny($currency){
		return $this->getParameter($currency);
	}
	public function getPaymentBrand($paymentBrand){
		return $this->getParameter($paymentBrand);
	}
	public function getPaymentType($paymentType){
		return $this->getParameter($paymentType);
	}
	public function getCardNumber($cardNumber){
		return $this->getParameter($cardNumber);
	}
	public function getCardHolder($cardHolder){
		return $this->getParameter($cardHolder);
	}
	public function getCardExpMonth($cardExpiryMonth){
		return $this->getParameter($cardExpiryMonth);
	}
	public function getCardExpYear($cardExpiryYear){
		return $this->getParameter($cardExpiryYear);
	}
	public function getCvv($cvv){
		return $this->getParameter($cvv);
	}

	public function curl_Connect() {
	    $url = $this->url."/".$this->verison."/".$this->api_;
		//$url = "https://test.oppwa.com/v1/payments";
		$data = "authentication.userId=" .$this->getUser().
			"&authentication.password=" .$this->getPassword().
			"&authentication.entityId=" .$this->getEntity().
			"&amount=" .$this->getAmount().
			"&currency=" .$this->getCurreny().
			"&paymentBrand=" .$this->getPaymentBrand().
			"&paymentType=" .$this->getPaymentType().
			"&card.number=" .$this->getCardNumber().
			"&card.holder=" .$this->getCardHolder().
			"&card.expiryMonth=" .$this->getCardExpMonth().
			"&card.expiryYear=" .$this->getCardExpYear().
			"&card.cvv=".$this->getCvv();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responseData = curl_exec($ch);
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		return $responseData;
	}
}