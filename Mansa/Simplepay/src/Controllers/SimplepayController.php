<?php 
namespace Mansa\Simplepay\Controllers;
 

use App\Http\Controllers\Controller;
use Mansa\Simplepay\Simplepay;

class SimplepayController extends Controller
{
    public $testEndpoint = "https://test.oppwa.com/";
    public $liveEndpoint = "https://oppwa.com/";
    public $verison = 'v1';
    public $api_ = '/payments';
    public $url ='';
    public $env = 'test';//live


    function getUrl(){
        $endpoint = $this->env == 'live'?$this->liveEndpoint:$this->testEndpoint;
        $url = $endpoint.$this->verison.$this->api_;
        return $url;

    }

    function getParams(){
        
        $settings = [];
        $settings['userId'] = '8a8294184e542a5c014e691d340708cc';
        $settings['password'] = '2gmZHAeSWK';
        $settings['entityId'] = '8a8294184e542a5c014e691d33f808c8';

        $settings['amount']='96.00';
        $settings['currency']='USD';
        $settings['paymentBrand']='VISA';
        $settings['paymentType']='PA';
        $settings['cardnumber']='4200000000000000';
        $settings['cardholder']='Mike Brandy';
        $settings['cardexpiryMonth']='06';
        $settings['cardexpiryYear']='2021';
        $settings['cardcvv']='123';
        $settings['environment']='test';
return $settings;
      
    }

    public function foo(){
//          return view('Simplepay::welcome');

    }
    public function SynCurl_Connect() {
        $url = $this->getUrl();
        $settings = $this->getParams();
        
        $data = "authentication.userId=" .$settings['userId'].
            "&authentication.password=" .$settings['password'].
            "&authentication.entityId=" .$settings['entityId'].
            "&currency={$settings['currency']}".
            "&amount={$settings['amount']}".
           
            "&paymentBrand=" .$settings['paymentBrand'].
            
            "&paymentType=" .$settings['paymentType'].
            "&card.number=" .$settings['cardnumber'].
            "&card.holder=" .$settings['cardholder'].
            "&card.expiryMonth=" .$settings['cardexpiryMonth'].
            "&card.expiryYear=" .$settings['cardexpiryYear'].
            "&card.cvv=".$settings['cardcvv'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return $responseData;
    }


    function getResponse(){

        $response = $this->SynCurl_Connect();
        $response = json_decode($response,true);

        echo "<pre>";
        var_dump($response);
    }


}


?>