<?php

// Route::get('simplepay/',
//   'Mansa\Simplepay\SimplepayController@callServer');

 Route::get('simplepay/pay',
   'Mansa\Simplepay\SimplepayController@paymentView');

 Route::get('simplepay/hello',
   'Mansa\Simplepay\SimplepayController@hello');

Route::get('/', function () {
    return Simplepay::saySomething();
});

Route::group(['namespace' => 'Mansa\Simplepay\Controllers', 'prefix'=>'simplepaydemo'], function() {
    // Your route goes here
        Route::get('foo', 'SimplepayController@foo');
        Route::get('paymentView','SimplepayController@getResponse');
});
// Route::get('simplepay/pay_response',
//   'Mansa\Simplepay\SimplepayController@getPaymentResponse');
?>